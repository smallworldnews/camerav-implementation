# 6. TECHNICAL DETAILS FOR USING CameraV
---

The ability to automatically verify these videos and images is important to human rights defenders, journalists, and regular people. These phones more often than not contain additional information that could compromise the person taking the image or video, but this same data can also provide valuable evidence that could help verify that something happened at a particular time and place. CameraV uses the built-in features of mobile phones to collect this data, protect it, and provide a mechanism by which to verify that it wasn’t tampered with. This preserves the chain of custody, adding to the evidentiary value of the images and videos captured.

Chapter 6 covers the sensors that CameraV taps into on the phone and the J3M format (JSON Mobile Media Metadata) used to store the data. It is not necessary to understand Chapter 6 in order to use CameraV. If you do not have a technical background, you may wish to ask a colleague with technical expertise to assist you with understanding the material in Chapter 6.

## SAMPLE J3M FILE

Below is an annotated version of the JSON data in a typical J3M bundle.

{“ASSET_PATH”: “SUBMISSIONS/45454AC1ADE36EBEC3749E8DC2AEDC4B”,

The “asset_path” represents where the file was originally stored on the CameraV app’s encrypted internal storage. This will rarely be used, but could be helpful in extreme situations where inspection of the capture device is necessary.

“GENEALOGY”: {“LOCALMEDIAPATH”: “/E61756A62A37535B77B0183318C- 79D26A2E0BDF0”, “HASHES”: [“9230DE4B067B2F14AFCAA41D23B30A09”], “J3M_VERSION”: “J3M VERSION 1.0”, “CREATEDONDEVICE”: >“694DB2C3EC- C07AC07F63E323F7B9A0CEFADA94CF”, “DATECREATED”: 1386690725995},

The Genealogy tag provides the basic data about the source of the media. “Hashes” is an MD5 hash of all the pixel values of the image or video frames. “createdOnDevice” is the OpenPGP public key  ngerprint for the user/app. “dateCreated” is a timestamp value for when the media capture occurred.

“FILE_NAME”: “KXERFDRNCHINOXAWWUGYEBKNBC.J3M”,

file_name is the name of the J3M  le as stored in the phone’s internal memory.

“PUBLIC_HASH”: “B840CBFD806865FFF8CC34078540224CFE804AE5”,

public_hash is a SHA-1 cryptographic hash that combines the user’s public key fingerprint and the MD5 media hash from above. This is used as the searchable public token identifier for the media file.

“INTENT”: {“ALIAS”: “AI WHITENESS”, “OWNERSHIPTYPE”: 400, “PGPKEYFINGERPRINT”: “694DB2C3ECC07AC07F63E323F7B9A0CEFADA94CF”, “INTENDEDDESTINATION”: “INFORMACAM TESTBED”}, “DATE_ADMITTED”: 1386726920279.5662, “ ID“:”86AE352E68328C06DE7840F4CB6BE809“,

Intent represents the alias of the person who captured it, again their PGP key  ngerprint, and who they meant to send this media file to, along with any record of it actually being transmitted. The “in- tendedDestination” information comes from any installed “trusted destination” or ICTD con guration  les, that are stored in the app.

“DATA”: { “SENSORCAPTURE”: [

The “data” section is where the sensor metadata logs are stored. It is an array of timestamped sensorCapture items.

{“TIMESTAMP”: 1386690720753, “CAPTURETYPE”: 271, “SENSORPLAYBACK”: {“AZIMUTHCORRECTED”: -1.84727144241333, “PITCHCORRECTED”: 0.017154498025774956, “AZIMUTH”: 43.07861328125, “PITCH”: >-18.8385009765625, “ROLL”: -132.7789306640625, “ROLLCORRECTED”: -0.12050031125545502}},

This is an orientation event, containing azimuth, pitch and roll, both in raw formats and “corrected” based on the orientation the user is holding their phone.

{“TIMESTAMP”: 1386690734267, “CAPTURETYPE”: 271, “SENSORPLAYBACK”: {“LIGHTMETERVALUE”: 13}},

This is a light meter value.

{“TIMESTAMP”: 1386690729261, “CAPTURETYPE”: 271, “SENSORPLAYBACK”: {“PRESSUREHPAORMBAR”: 1007.3463134765625, “LIGHTMETERVALUE”: 10, “PRESSUREALTITUDE”: 49.26783752441406}},

This is a combined event with light meter and pressure data, both raw, and adjusted based on the phone’s indicated local elevation.

{“TIMESTAMP”: 1386690729939, “CAPTURETYPE”: 271, “SENSORPLAY- BACK”: {“VISIBLEWIFINETWORKS”: [{“BSSID”: “28:C6:8E:BA:EA:DC”, “WIFIFREQ”: 5220, “WIFILEVEL”: -93, “BT_HASH”: >“AFBF1E7FFC07F- 6B4471E34F8470F5FDE947A8F2B”, “SSID”: “CLOUDCITY5GHZ”}, {“BSSID”: “1C:AF:F7:D8:DB:61”, “WIFIFREQ”: 2462, “WIFILEVEL”: -88, “BT_HASH”: >“9C1CB7186BEA393589AC3A591052F91DA423205E”, “SSID”: “CLOUD10”}, {“BSSID”: “28:C6:8E:BA:EA:DA”, “WIFIFREQ”: 2437, “WIFILEVEL”: -61, “BT_HASH”: “7B3B34FE541048F0E0800F1B788DC44CFDF6A59D”, >“SSID”: “CLOUDCITY”},...

This is a “visibleWi Networks” event capture displaying network names, frequency, strength and MAC address information.

{“TIMESTAMP”: 1386690719706, “CAPTURETYPE”: 271, “SENSORPLAYBACK”: {“GPS_COORDS”: [-71.1253508, 42.3286856], “GPS_ACCURACY”: “32.119”}},

This is a GPS location event, display latitude, longitude and current accuracy of the sensor, based on whether it is coming from satellite, WiFi, cell towers, etc.

{“TIMESTAMP”: 1386690721758, “CAPTURETYPE”: 271, “SENSORPLAYBACK”: {“ACC_Z”: 9.188077926635742, “ACC_Y”: 2.7202823162078857, “ACC_X”: -1.9511220455169678}},

This is an accelerometer event, showing X,Y,Z motion data.

{“TIMESTAMP”: 1386690719714, “CAPTURETYPE”: 271, “SENSORPLAYBACK”: {“BLUETOOTHDEVICEADDRESS”: “5D9D203488950FF20C07B6DBFE9A8B8DDA- BAFC6C”, “LAC”: “36493”, “MCC”: “310260”, >“BLUETOOTHDEVICENAME”: “NEXUS 4”, “CELLTOWERID”: “79211356”}},

This is a telephony event, showing both any bluetooth devices noticed in the area, and information about the cellular network tower the smartphone is currently registered with. If the device is a WiFi only device, or is not using a SIM card, this data will simply be omitted. The Bluetooth device address does NOT display the name of the actual device MAC address, but instead shows a one-way hash value. This was an attempt to preserve some privacy for individuals who might be in the area.

After the sensor data, the J3M then shows basic “EXIF” style information from the capture device:

“EXIF”: {“ORIENTATION”: 0, “FOCALLENGTH”: -1, “TIMESTAMP”: “2013:12:10 10:51:48”, “MAKE”: “LGE”, “FLASH”: -1, “HEIGHT”:
960, “WIDTH”: 1280, “ISO”: “100”, “LOCATION”: [-71.1250228881836, >42.32872772216797], “DURATION”: 0, “MODEL”: “NEXUS 4”, “EXPOSURE”: “0.033”, “WHITEBALANCE”: -1, “APERTURE”: “2.7”},

Finally, any user annotations, based on Open Data Kit forms provided as part of the “Trusted Destination”  le, are shown here:

The form definition used is indicated, and a basic free text annotation is shown here:

“USERAPPENDEDDATA”: [{“ASSOCIATEDFORMS”: [{“PATH”: “/FORMS/493DDE68C 49E6B99556186A3E776D705.XML”, “NAMESPACE”: “IWITNESS FREE TEXT ANNOTATIONS”, “ID”: “234D025EE64976D27E1D2305F80824BC”, >“ANSWERDATA”: {“IW_FREE_TEXT”: “WATCH OUT FOR ICY SIDEWALKS AND ROADS”}}], “TIMESTAMP”: 1386690794797, “ID”: “CDB7C22265121160DEC- 5C0598263F58C”}, {“ASSOCIATEDFORMS”: [{“PATH”: >“/FORMS/493DDE68C49E 6B99556186A3E776D705.XML”, “NAMESPACE”: “IWITNESS FREE TEXT ANNOTATIONS”, “ID”: “B63A2A65FC91DD9744D6CD5CEA5CB28D”, “ANSWERDATA”: {“IW_FREE_TEXT”: “THIS TREE MIGHT >FALL DOWN”}},

If an annotation is placed at speci c X,Y point in the image, or X,Y+time window for video, that information is also provided:

{“PATH”: “/FORMS/46B9F8E70113AE0F39AE26338C0DC433.XML”, “NAMESPACE”: “IWITNESS V 1.0”, “ID”: “FAE0900EEC13BAEFCE4F98B895B80405”, “ANSWERDATA”: {“IW_INDIVIDUAL_IDENTIFIERS”: “VICTIM”}}], “TIMESTAMP”: 1386690798758, “REGIONBOUNDS”: {“TOP”: 224, “DISPLAYLEFT”: 415, “HEIGHT”: 118, “WIDTH”: -37,
“DISPLAYWIDTH”: 115, >“STARTTIME”: -1, “DISPLAYTOP”: 224, “DISPLAYHEIGHT”: 118, “ENDTIME”: -1, “LEFT”: 263}, “ID”: “1E9D35BED92B8FDFE46B251AFB3227F2”, “INDEX”: 0}, {“ASSOCIATEDFORMS”: [{“PATH”: >“/FORMS/493DDE68C49E6B99556186A3E77 6D705.XML”, “NAMESPACE”: “IWITNESS FREE TEXT ANNOTATIONS”, “ID”: “B63A2A65FC91DD9744D6CD5CEA5CB28D”, “ANSWERDATA”: {“IW_FREE_TEXT”: “THIS TREE MIGHT >FALL DOWN”}}, {“PATH”: “/FORMS/46B9F8E70113A E0F39AE26338C0DC433.XML”, “NAMESPACE”: “IWITNESS V 1.0”, “ID”: “FAE0900EEC13BAEFCE4F98B895B80405”, “ANSWERDATA”: {“IW_INDIVIDUAL_IDENTIFIERS”: >“VICTIM”}}], “TIMESTAMP”: 1386690798758, “REGIONBOUNDS”: {“TOP”: 224, “DISPLAYLEFT”: 415, “HEIGHT”: 118, “WIDTH”: -37, “DISPLAYWIDTH”: 115, “STARTTIME”: -1, “DISPLAYTOP”: 224, >“DISPLAYHEIGHT”: 118, “ENDTIME”: -1, “LEFT”: 263}, “ID”: “1E9D35BED92B8FDFE46B251AFB3227F2”, “INDEX”: 0}]}}

## IMPOSSIBLE VALUES

To easily translate values from JSON into our database, certain values must not be null, or NaN, but must be given impossible values that still adhere to the expected type. The following notes apply to specific values:
- Cell Tower ID, if unknown, will be recorded as -1.
- Location data, if unknown, will be recorded as [0.0, 0.0]. This is technically a legitimate latitude and longitude, but should be regarded as NaN. Note the Android client always reports position with up to 9-decimal place precision; should a client actually report from this location, the actual reading would be similar to 0.000000134.

## J3M SPECIFICATIONS

The metadata captured by CameraV comes from different chips and sensor components in the smartphone. The table on the next page details how different pieces of data are generated, and where they come from.

[first image - or maybe a table?](/images/)
