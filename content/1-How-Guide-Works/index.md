# How the Guide Works
---
This guide is aimed at human rights defenders engaged in high-risk documentation work. It is split into six chapters. Chapters 1 and 2 provide an overview to the CameraV application, and a walkthrough of CameraV’s main features and important settings. Chapter 3 will introduce you to CameraV’s unique capabilities for increasing the veracity of your media. Chapters 4-5 attempt to answer any questions you may have about using CameraV safely in your work as human right defenders. Chapter 6 details technical speci cations about CameraV’s functionality. Chapter 6 is only for those with a high degree of technical expertise. In Chapters 1-5 you will find some words have been underlined. Definitions for these terms can be found in the Glossary at the end of the guide.

If you are new to human rights work, or to the CameraV application, we recommend you read the first five chapters in their entirety. If you are familiar with CameraV, you may choose to skip to Chapter 3, or even Chapter 4.

## Please note the use of this icon

![first image](images/)

This icon denotes potential risks, and suggests solutions where possible.

![second image](images/)
