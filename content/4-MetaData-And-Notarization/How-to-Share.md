# HOW TO SHARE METADATA

From the Media View, you can select the ‘Share’ action, and then the ‘Share Metadata’ from the second menu. This will generate a plain text document (more on this below), that you can share with any app on your device that will accept that kind of content.

You can also ‘Share Metadata’ from the Gallery view, and share multiple metadata documents in batch, using the multi-select capability.

Remember Bob? He is still focused on documenting police harassment and violence in his community. Today he is at a rally in his neighborhood protesting police abuses. During the rally, demonstrators decide to lead an unpermitted march. Police attack that march and Bob captures video and photo of the events. Eventually he finds himself in a group of protesters who are surrounded. Fearing he may be arrested and the photo and video might be deleted, he shares the content with his friend Dave, who is back at the rally. CameraV allows him to use WhatsApp to share his photos and video to Dave, combined with a notarization message via SMS that contains the metadata of the footage confirming where and when the photos and video were captured.

[first image](/images/)
