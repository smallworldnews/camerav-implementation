# GLOSSARY
---
The glossary definitions are largely selected from Wikipedia, we encourage you to do your own research to ensure you understand all terms fully. Terms appearing only in Chapter 6 have been left out of the glossary.

## ACCELEROMETER
An accelerometer is a device that measures proper acceleration; proper acceleration is not the same as coordinate acceleration (rate of change of velocity). Some smartphones, digital audio players, and personal digital assistants contain accelerometers for user interface control; often the accelerometer is used to present landscape or portrait views of the device's screen, based on the way the device is being held. Along with orientation view adjustment, accelerometers in mobile devices can also be used as pedometers, in conjunction with specialized applications.

## CameraV
CameraV is an smartphone application for creating veri able photos and video clips. CameraV uses smartphones’ built-in sensors for tracking movement, light and other environmental inputs, along with WiFi, Bluetooth, and cellular network metadata to capture a snapshot of the environment while you are taking a photo or video. The metadata is attached to each photo or video to increase its veracity.

## CELL TOWER
A cell site or cell tower is a cellular telephone site where antennae and electronic communications equipment are placed, usually on a radio mast, tower, or other high place, to create a cell (or adjacent cells) in a cellular network. The elevated structure typically supports antennae, and one or more sets of transmitter/receivers transceivers, digital signal processors, control electronics, a GPS receiver for timing, primary and backup electrical power sources, and sheltering.

## DIGITAL FINGERPRINT
In computer science, a fingerprinting algorithm is a procedure that maps an arbitrarily large data item (such as a computer file) to a much shorter bit string, its (digital) fingerprint, that uniquely identifies the original data for all practical purposes just as human fingerprints uniquely identify people for practical purposes. This fingerprint may be used for data deduplication purposes.

Fingerprints are typically used to avoid the comparison and transmission of bulky data. For instance, a web browser or proxy server can efficiently check whether a remote file has been modi ed, by fetching only its fingerprint and comparing it with that of the previously fetched copy.

## ENCRYPTION
Encryption is the process of encoding messages or information in such a way that only authorized parties can read it. Encryption does not of itself prevent interception, but denies the message content to the interceptor. In an encryption scheme, the intended communication information or message, referred to as plaintext, is encrypted using an encryption algorithm, the text that results from encryption is known as ciphertext.

The purpose of encryption is to ensure that only somebody who is authorized to access data (e.g. a text message or a file) will be able to read it, using the decryption key. Somebody who is not authorized can be excluded, because he or she does not have the required key, without which it is impossible to read the encrypted information.

## EXIF
Exchangeable image file format (officially Exif, according to JEIDA/JEITA/CIPA specifications) is a standard that specifies the formats for images, sound, and additional tags used by digital cameras (including smartphones), scanners, and other systems handling image and sound files recorded by digital cameras.

## GPS
GPS, which stands for Global Positioning System, is a radio navigation system that allows land, sea, and airborne users to determine their exact location, velocity, and time 24 hours a day, in all weather conditions, anywhere in the world.

## HASH
In this guide the term hash is related to hash functions. A hash function is any function that can be used to map data of arbitrary size to data of fixed size. The values returned by a hash function are called hash values, hash codes, hash sums, or simply hashes. In CameraV hashes are used to create a unique, shortened string of characters that refers to a much longer string of characters that provides the entirety of a photo or video’s metadata. These hashes ensure that what is otherwise a very long document can be transferred in a unique, much shorter text.

## J3M
J3M is an implementation of JSON used by CameraV to organize and store the metadata associated with images and video clips. It is a plain text, human readable format, that are various groups of name=value pairs, grouped in brackets like {name: value}, or {name { name0: value0, name1: value1}.

## JSON
(JavaScript Object Notation) is an open-standard format that uses human-readable text to transmit data objects consisting of attribute–value pairs. It is the most common data format used for asynchronous browser/server communication, largely replacing XML which is used by AJAX.

CameraV uses an implementation of JSON called J3M to organize and store the metadata associated with images and video clips.

## MEDIA
This guide uses media primarily to mean digital media. Digital media are any media that are encoded in a machine-readable format. Digital media can be created, viewed, distributed, modi ed and preserved on digital electronics devices. Computer programs and software; digital imagery, digital video; video games; web pages and websites, including social media; data and databases; digital audio, such as mp3s; and e-books are examples of digital media. Digital media are frequently contrasted with print media, such as printed books, newspapers and magazines, and other traditional or analog media, such as pictures, film or audio tape.

## METADATA
Metadata is a set of data that describes and gives information about other data. For example, a digital image may include metadata that describes how large the picture is, the color depth, the image resolution, when the image was created, the shutter speed, and other data. This is all metadata.

## NOTARIZATION
Notarization is the act of attaching the digital fingerprint provided by CameraV to a specific image or video to prove it was created with that specific device.

## PASSPHRASE
A passphrase is a sequence of words or other text used to control access to a computer system, program or data. A passphrase is similar to a password in usage, but is generally longer for added security. Passphrases are often used to control both access to, and operation of, cryptographic programs and systems- especially those that derive an encryption key from a passphrase. The origin of the term is by analogy with password. The modern concept of passphrases is believed to have been invented by Sigmund N. Porter in 1982.

## PGP
Pretty Good Privacy (PGP) is an encryption program that provides cryptographic privacy and authentication for data communication. PGP is often used for signing, encrypting, and decrypting texts, e-mails, files, directories, and whole disk partitions and to increase the security of e-mail communications. It was created by Phil Zimmermann in 1991.

## SENSOR
A sensor is a device that measures a physical quantity and converts it into a signal which can be read by an observer or by an instrument. In a smartphone, sensors include the GPS and accelerometer, each of which measures different data that can be accessed by apps and the device itself for a variety of unique uses.

## TIMESTAMP
A timestamp is a sequence of characters or encoded information identifying when a certain event occurred, usually giving date and time of day, sometimes accurate to a small fraction of a second. The term derives from rubber stamps used in offices to stamp the current date, and sometimes time, in ink on paper documents, to record when the document was received. Common examples of this type of timestamp are a postmark on a letter or the "in" and "out" times on a time card.
