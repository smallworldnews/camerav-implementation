# POLICE CONFISCATION

## WHAT IF MY PHONE IS TAKEN BY A POLICE OFFICER OR A MEMBER OF ANOTHER SECURITY SERVICE?

If you are documenting abuses by government officials, members of the police, or security services, you are at a much higher risk of arrest. Human rights defenders in particular are at risk of harassment and the content they document is often of exceptional interest to government agents. It is now common practice to search mobile devices at borders, in military zones, and areas under martial law or other emergency procedures.

This is one of the reasons that CameraV contains a fully encrypted storage capability, which appears as a single  file on the device storage. Let's examine the following scenario that will better explain.

[first image](/images/)

Human rights defender Jonathan returned from the  eld with unreliable data. He interviewed four witnesses to a land dispute. He took notes in a paper notebook that he brought with him to document the names of witnesses, those a affected, and contact information for community members willing to speak further. On his way home he met a person on the road who said there was a military checkpoint nearby. Jonathan was told the checkpoint was in response to the incident he was investigating. Faced with this challenge, Jonathan decided it was too risky to pass with his documentation and destroyed his notes. He was held at the checkpoint for an hour, thoroughly searched, but was eventually allowed to pass. Jonathan was unable to remember the witness testimony without his notes, nor could he recall all of the contact information he recorded.

Shortly after, he was able to return to the area and salvage some of the testimony by interviewing the witnesses again to record their statements and consent on camera. On the advice of a colleague, Jonathan purchased a smartphone and downloaded the CameraV app to take with him. After he completed his recordings, he notarized each clip, and sent a fingerprint of each of the video files to a contact back at his office. Sending the original video clips was considered too risky and bandwidth was also not enough to send the clips quickly. Instead, Jonathan implemented CameraV’s function to “Enable Alternative Icon” so that the app would not be obvious to a cursory review. Then he stored the device with a local who could bring it past the checkpoint without suspicion. Once the device was returned to him, he passed the data to a human rights lawyer who was investigating the case.

If the police are aware of CameraV, they may arrest you and try to force you to open the application in order to reveal your recorded content. If you believe this may be a risk, consider using the “Enable Alternative Icon” function mentioned previously. However, the best defense is to find a way to transport your device with the least risk of discovery. That is why Jonathan gave his device to a local who would not be suspected of documenting human rights violations.

CameraV uses the IOCipher virtual file system for this, and enables the media and metadata to be directly stored in encrypted format, without ever being stored unencrypted on the device. This is important, since once you store something unencrypted on a mobile device, you open it up to being copied if your device is taken and forensically recovered at a future time, or intercepted by other apps.

These last two highlight additional risks you will face when using a mobile device for documentation.

Because of the type of storage they use, it is extremely difficult to completely delete files from a mobile device. CameraV provides a solution by keeping the files in a specific, encrypted container, enabling a user to delete all the files in that specific place.

![second image](/images/)

Malicious actors are increasingly creating apps that masquerade as games or useful tools, but are in fact used to infect your device with spyware or malware. If you download one of these apps it can access content stored in your gallery and elsewhere in your device’s standard internal and external storage. Filea stored in CameraV’s encrypted container can only be accessed by entering your passphrase to decrypt the data.
