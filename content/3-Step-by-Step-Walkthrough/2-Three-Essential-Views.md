# THREE ESSENTIAL VIEWS

![first image summary table](images/)

## THE HOME VIEW

The Home View is what you are presented with when you enter the app for the first time. It allows you to quickly see the last few items captured, and swipe through all of them. You can tap on any media preview image to switch to the Media View, which will show the full photo or video.

![second image](/images)

This view is also where you open the camera or navigate to the Gallery view. The three buttons at the bottom of the screen represent the photo camera, video camera and gallery, in that order. The camera buttons launch the camera for capturing media in the indicated format. The gallery button opens the Gallery View for filtering and batch processing of media.

![third image](/images/)

You can also lock the app from the menu option to sign out and remove all cached information from memory. The app will
then require you to enter a passphrase the next time you open it. Finally, there is the “Panic Button” action available on this screen, from which you can quickly erase all media and app data from CameraV. CameraV’s security capabilities will be discussed in detail in Chapter 4.

![fourth image](/images/)

![fifth image](images/)

## THE MEDIA VIEW

The Media View enables the user to view a captured photo or video in its entirety. You can see the entire photo and pinch and zoom it for a closer look. If it is a landscape image you can rotate your phone into landscape mode to see the photo with more detail. If it is a video you can watch the video in its entirety. It will take a few seconds to load, after that you can press play to view the video.

![sixth image](/images/)

At the top of the Media View, you will see four action icons: the Informa View “I”, Metadata, the Share action, and the Edit action shown as a Pencil icon.

The Informa View provides full access to the raw sensor metadata captured when you captured the picture or video. It is shown in the “J3M” format detailed in Chapter 6, which provides detail on CameraV’s technical specifications.

![seventh image](/images/)

Tapping on the Share action will reveal three more options: Share Media, Share Metadata, and Notarize. The Notarize action is discussed in detail in Chapter 3.

![eighth image](/images/)

Share Media will package the media file metadata, and generate a new JPEG or MPEG-4 file that can be shared with any app, sent via email, posted on social networks, or uploaded to a website.

‘Notarize’ shares a short snippet of text containing the media file’s unique visual fingerprint, that can be easily shared via SMS, email, Twitter or other short message channel. This enables the user to timestamp the media with a third-party, and provide a mechanism to detect any tampering or modi cation of the media at a later time. For more detail on how notarization works, see Chapter 3.

![ninth image](/images/)

Tapping on the ‘Edit’ action will allow you to ‘Write Text’ in order to provide a basic description of what the captured media item is about, or other context you wish to provide. It does not allow you to edit the image or video. ‘Add Tags’ will allow you to place a tag box on the media at a certain place or time-in the case of video. You can tap on the tag box to enter more detailed information about what you were tagging, and why.

You can return to the Home View by pressing the back arrow in the top, left corner of the screen, or by pressing the back key on your device.

## THE GALLERY VIEW

![tenth image](/images/)

The Gallery View provides a thumbnail view of many media items at once. You can filter media items by type to see only photos, only videos, or all. You can also select multiple media items for batch operations by using the checkmark action or pressing on one item and holding for a few seconds. This will engage the multi-select mode, which allows for multiple items to be selected at once. You can then do a batch export and share action or delete action. The Share options provided are the same as in the Media View: Share Media, Share Metadata, and Notarize.
