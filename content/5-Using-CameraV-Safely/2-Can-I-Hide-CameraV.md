# CAN I HIDE CameraV

## CAN I HIDE THE CameraV APPLICATION IN THE EVENT SOMEONE WANTS TO USE MY PHONE OR A POLICE OFFICER WANTS TO EXAMINE IT?

The work of human rights defenders can be extremely sensitive. Often they do not want even their close friends or family to know they are involved in human rights work. Additionally, such individuals are at risk of being stopped and searched by police officers and members of other security services.

Currently you can use an “Alternative Icon” which looks similar to a generic “Settings” icon. From the home screen in CameraV, press the menu button at the top of the screen, which looks like 3 vertical dots. Select “Settings” and then check the box next to “Enable Alternative Icon.” Once you do this, CameraV will now resemble a mechanical gear when you look for it in your applications tray. It will no longer be called “CameraV” but will instead be called “Settings.”

[first image](/images/)

If someone is determined to search your phone and examine every application, it is not currently possible to completely hide CameraV in a safe manner. Some apps exist that can hide themselves, but this feature has not yet been added to CameraV. Other apps exist that can hide another app, but these involve additional steps to modify the phone’s operating system which may create additional risks.
