# WORKING WITH METADATA AND NOTARIZATION
---
Metadata is automatically recorded every time you capture a photo or video. Chapter 3 will show you how to share the metadata associated with every photo and video you’ve captured using the app. Metadata is all the information CameraV captures about the photo or video, such as the time it was taken, GPS coordinates for the content, and the device’s individual fingerprint. In addition to the metadata, Chapter 3 will introduce another key feature of CameraV called notarization. Notarization is used to prove an individual photo or video was really created by a specific device.

The metadata you can share from CameraV is the collection of sensor and network event data captured around the time that the photo or video was captured. It is organized and store using the Javascript Object Notation (JSON) format, and more specifically, as JSON Mobile Media Metadata, or J3M (pronounced as in “gem”). The J3M file is a plain text, human readable format, that are various groups of name=value pairs, grouped in brackets like {name: value}, or {name { name0: value0, name1: value1}, and so on. For technical details about the metadata please see Chapter 6.

[first image](/images/)
